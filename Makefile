DOC = embedded-linux-from-scratch-riscv.tex

INKSCAPE_IS_NEW = $(shell inkscape --version | grep -q "^Inkscape 1" && echo YES)

ifeq ($(INKSCAPE_IS_NEW),YES)
INKSCAPE_PDF_OPT = -o
else
INKSCAPE_PDF_OPT = -A
endif

PICTURES = \
	common/logo-penguins.pdf \
	common/bootlin-logo.pdf \
	common/logo-square.pdf \
	graphics/milkv-duos.pdf \
	graphics/logo-cc.pdf \
	graphics/RISC-V-logo.pdf \
	graphics/cross-toolchain.pdf \
	graphics/riscv-boot.pdf \
	graphics/system-boot.pdf \
	graphics/initramfs.pdf \
	graphics/you-are-gurus.pdf \

all: $(PICTURES)
	$(PDFLATEX_ENV) xelatex -shell-escape $(DOC)

%.pdf: %.svg
	inkscape -D $(INKSCAPE_PDF_OPT) $@ $<

%.pdf: %.eps
	epstopdf --outfile=$@ $^

%.eps: %.dia
	dia -e $@ -t eps $^

clean:
	$(RM) -r common/*.pdf *.pdf *.pyg *.snm *.toc *.vrb *.aux *.nav *.out *.dia~ *.log _minted* $(PICTURES)
